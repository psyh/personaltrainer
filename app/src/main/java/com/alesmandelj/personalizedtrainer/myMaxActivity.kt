package com.alesmandelj.personalizedtrainer

import android.content.Intent
import android.content.pm.ActivityInfo
import android.media.AudioManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_my_max.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Locale

class myMaxActivity : AppCompatActivity() {



    //adding maximum lifts to database
    //adding maximum lifts to history

    fun cancelFunction(view: View) {

        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }


    fun CheckEditText(): Boolean {

        var checkEditText = true

        if (TextUtils.isEmpty(editText1.text.toString())) {
            editText1.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText2.text.toString())) {
            editText2.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText3.text.toString())) {
            editText3.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText4.text.toString())) {
            editText4.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText5.text.toString())) {
            editText5.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText6.text.toString())) {
            editText6.error = "No value input"
            checkEditText = false
        }
        if (TextUtils.isEmpty(editText7.text.toString())) {
            editText7.error = "No value input"
            checkEditText = false
        }
        return checkEditText
    }

    fun saveMax(view: View) {

        val checkedEditTexts = CheckEditText()

        if (checkedEditTexts == false) {

            Toast.makeText(this, "Maximum lifts not saved!", Toast.LENGTH_SHORT).show()
        } else if (MainActivity.unit == "lbs") {

            //put current date to string

            val today = Calendar.getInstance().time
            val formatter = SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault())
            val maxTime = formatter.format(today)

            val maxStringList = ArrayList<Int>()

            try {
                val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)

                val maxIndex = c.getColumnIndex("maxLift")
                var count = 0

                c.moveToFirst()

                while (count < 7) {

                    maxStringList.add(c.getInt(maxIndex))
                    c.moveToNext()
                    count++
                }

            } catch (e: Exception) {

                e.printStackTrace()
            }

            if (maxStringList.isEmpty()) {

                var st = 0

                while (st < 7) {

                    maxStringList.add(0)
                    st++
                }
            }

            try {

                MainActivity.maxDB.execSQL("DROP TABLE myMax")

                MainActivity.maxDB.execSQL("CREATE TABLE IF NOT EXISTS myMax (id INTEGER PRIMArY KEY, exercise STRING, maxLift INT)")

                //Add to max database

                val enaa = Integer.parseInt(editText1.text.toString()) / 2.2046226218488
                val ena = enaa.toInt()
                val dvaa = Integer.parseInt(editText2.text.toString()) / 2.2046226218488
                val dva = dvaa.toInt()
                val trii = Integer.parseInt(editText3.text.toString()) / 2.2046226218488
                val tri = trii.toInt()
                val stirii = Integer.parseInt(editText4.text.toString()) / 2.2046226218488
                val stiri = stirii.toInt()
                val pett = Integer.parseInt(editText5.text.toString()) / 2.2046226218488
                val pet = pett.toInt()
                val sestt = Integer.parseInt(editText6.text.toString()) / 2.2046226218488
                val sest = sestt.toInt()
                val sedemm = Integer.parseInt(editText7.text.toString()) / 2.2046226218488
                val sedem = sedemm.toInt()


                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Bench press: ', $ena)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Deadlift: ', $dva)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Front squat: ', $tri)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Back squat: ', $stiri)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Clean: ', $pet)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Clean and Jerk: ', $sest)")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Snatch: ', $sedem)")

                //Add to max history by exercise

                if (maxStringList[0].toBigDecimal() != BigDecimal(Integer.parseInt(editText1.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO bench(maxLift,datum)VALUES ($ena,'$maxTime')")
                }
                if (maxStringList[1].toBigDecimal() != BigDecimal(Integer.parseInt(editText2.text.toString()) / 2.2046226218488) ){

                    MainActivity.maxDB.execSQL("INSERT INTO deadlift(maxLift,datum)VALUES ($dva,'$maxTime')")
                }
                if (maxStringList[2].toBigDecimal() != BigDecimal(Integer.parseInt(editText3.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO fSquat(maxLift,datum)VALUES ($tri,'$maxTime')")
                }
                if (maxStringList[3].toBigDecimal() != BigDecimal(Integer.parseInt(editText4.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO bSquat(maxLift,datum)VALUES ($stiri,'$maxTime')")
                }
                if (maxStringList[4].toBigDecimal() != BigDecimal(Integer.parseInt(editText5.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO clean(maxLift,datum)VALUES ($pet,'$maxTime')")
                }
                if (maxStringList[5].toBigDecimal() != BigDecimal(Integer.parseInt(editText6.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO cleanJerk(maxLift,datum)VALUES ($sest,'$maxTime')")
                }
                if (maxStringList[6].toBigDecimal() != BigDecimal(Integer.parseInt(editText7.text.toString()) / 2.2046226218488)) {

                    MainActivity.maxDB.execSQL("INSERT INTO snatch(maxLift,datum)VALUES ($sedem,'$maxTime')")
                }

                Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show()

                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()

            } catch (e: Exception) {

                e.printStackTrace()

            }

        } else if (MainActivity.unit == "kg") {

            //put current date to string

            val today = Calendar.getInstance().time
            val formatter = SimpleDateFormat("dd/MMM/yyyy", Locale.getDefault())
            val maxTime = formatter.format(today)

            val maxStringList = ArrayList<Int>()

            try {
                val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)

                val maxIndex = c.getColumnIndex("maxLift")
                var count = 0

                c.moveToFirst()

                while (count < 7) {

                    maxStringList.add(c.getInt(maxIndex))
                    c.moveToNext()
                    count++
                }

            } catch (e: Exception) {

                e.printStackTrace()
            }

            if (maxStringList.isEmpty()) {

                var st = 0

                while (st < 7) {

                    maxStringList.add(0)
                    st++
                }
            }

            try {

                MainActivity.maxDB.execSQL("DROP TABLE myMax")

                MainActivity.maxDB.execSQL("CREATE TABLE IF NOT EXISTS myMax (id INTEGER PRIMArY KEY, exercise STRING, maxLift INT)")

                //Add to max database

                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Bench press: ', " + Integer.parseInt(editText1.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Deadlift: ', " + Integer.parseInt(editText2.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Front squat: ', " + Integer.parseInt(editText3.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Back squat: ', " + Integer.parseInt(editText4.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Clean: ', " + Integer.parseInt(editText5.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Clean and Jerk: ', " + Integer.parseInt(editText6.text.toString()) + ")")
                MainActivity.maxDB.execSQL("INSERT INTO myMax (exercise, maxLift)VALUES ('Snatch: ', " + Integer.parseInt(editText7.text.toString()) + ")")

                //Add to max history by exercise

                if (maxStringList[0] != Integer.parseInt(editText1.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO bench(maxLift,datum)VALUES (" + Integer.parseInt(editText1.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[1] != Integer.parseInt(editText2.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO deadlift(maxLift,datum)VALUES (" + Integer.parseInt(editText2.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[2] != Integer.parseInt(editText3.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO fSquat(maxLift,datum)VALUES (" + Integer.parseInt(editText3.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[3] != Integer.parseInt(editText4.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO bSquat(maxLift,datum)VALUES (" + Integer.parseInt(editText4.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[4] != Integer.parseInt(editText5.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO clean(maxLift,datum)VALUES (" + Integer.parseInt(editText5.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[5] != Integer.parseInt(editText6.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO cleanJerk(maxLift,datum)VALUES (" + Integer.parseInt(editText6.text.toString()) + ",'" + maxTime + "')")
                }
                if (maxStringList[6] != Integer.parseInt(editText7.text.toString())) {

                    MainActivity.maxDB.execSQL("INSERT INTO snatch(maxLift,datum)VALUES (" + Integer.parseInt(editText7.text.toString()) + ",'" + maxTime + "')")
                }

                Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show()

                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)
                finish()

            } catch (e: Exception) {

                e.printStackTrace()

            }


        }
    }

    override fun onBackPressed() {

        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_max)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        title = "Enter your max!"
        volumeControlStream = AudioManager.STREAM_MUSIC



        val editMax = ArrayList<String>()

        val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)
        try {

            val ageIndex = c.getColumnIndex("maxLift")
            var count = 0
            c.moveToFirst()

            while (count < 7) {

                editMax.add(c.getString(ageIndex))
                count++
                c.moveToNext()
            }


            if (MainActivity.unit == "kg") {
                editText1.setText(editMax[0])
                editText2.setText(editMax[1])
                editText3.setText(editMax[2])
                editText4.setText(editMax[3])
                editText5.setText(editMax[4])
                editText6.setText(editMax[5])
                editText7.setText(editMax[6])
            } else if (MainActivity.unit == "lbs") {

                val enaa = Integer.parseInt(editMax[0]) * 2.2046226218488
                val ena = Math.round(enaa / 5).toInt() * 5
                val dvaa = Integer.parseInt(editMax[1]) * 2.2046226218488
                val dva = Math.round(dvaa / 5).toInt() * 5
                val trii = Integer.parseInt(editMax[2]) * 2.2046226218488
                val tri = Math.round(trii / 5).toInt() * 5
                val stirii = Integer.parseInt(editMax[3]) * 2.2046226218488
                val stiri = Math.round(stirii / 5).toInt() * 5
                val pett = Integer.parseInt(editMax[4]) * 2.2046226218488
                val pet = Math.round(pett / 5).toInt() * 5
                val sestt = Integer.parseInt(editMax[5]) * 2.2046226218488
                val sest = Math.round(sestt / 5).toInt() * 5
                val sedemm = Integer.parseInt(editMax[6]) * 2.2046226218488
                val sedem = Math.round(sedemm / 5).toInt() * 5



                editText1.setText(ena.toString())
                editText2.setText(dva.toString())
                editText3.setText(tri.toString())
                editText4.setText(stiri.toString())
                editText5.setText(pet.toString())
                editText6.setText(sest.toString())
                editText7.setText(sedem.toString())


            }

        } catch (e: Exception) {

            e.printStackTrace()
        }


        textView1.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView1.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView2.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView2.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView3.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView3.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView4.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView4.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView5.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView5.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView6.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView6.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
        textView7.setOnClickListener {
            val exe1 = ExerciseInfoClass()
            exe1.textViewexercise = textView7.text.toString()
            exe1.mContext = applicationContext
            exe1.showExercise()
        }
    }
}
