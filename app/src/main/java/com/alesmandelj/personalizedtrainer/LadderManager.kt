package com.alesmandelj.personalizedtrainer

import android.util.Log
import java.util.*
import kotlin.math.roundToLong

object LadderManager {

    private val bodyweightArray = ArrayList(Arrays.asList("Pull up: ", "Burpee: ", "Push up: ", "Toes to bar: ", "Air squat: ", "Box jump: ", "Pistols: ", "Muscle up: ", "Ring muscle up: ", "Handstand push up: "))


    fun generateLadder(): ArrayList<ExerciseObjectClass> {
        var weightLiftMax = 0
        val exerciseArray = ArrayList<ExerciseObjectClass>()
        val rand = Random()
        var counter = 0


        var index =rand.nextInt(7)
        var bodyWeightExercise: String


        //weightlifting exercises

        if (MainActivity.unit == "kg") {


            if (MainActivity.difficulty == "easy") {

                bodyWeightExercise = ExerciseManager.bodyweightArray[rand.nextInt(bodyweightArray.size - 3)]

            } else {
                bodyWeightExercise = ExerciseManager.bodyweightArray[rand.nextInt(bodyweightArray.size)]
            }

            try {

                val db = MainActivity.maxDB
                val selectQuery = "SELECT * FROM myMax "
                val c=db.rawQuery(selectQuery,null)
                val exercise = c.getColumnIndex("exercise")
                val weight = c.getColumnIndex("maxLift")

                c.moveToPosition(index)

                if (Integer.parseInt(Integer.parseInt(c.getString(weight)).toString()) == 0) {

                    weightLiftMax = 50
                } else {
                    weightLiftMax = c.getInt(weight)
                }

                    val weightLiftExercise = c.getString(exercise)

                    var ladderObjectClass: ExerciseObjectClass

                Log.d("LadderManager:  ","$weightLiftExercise")
                while (counter < 7) {

                    val weightUsed = 5 * (((weightLiftMax * (0.3 + counter * 0.1) / 5).roundToLong()))

                    ladderObjectClass = ExerciseObjectClass("$weightLiftExercise $weightUsed ${MainActivity.unit}", "x " + (14 - counter * 2) + "")
                    exerciseArray.add(ladderObjectClass)

                    ladderObjectClass = ExerciseObjectClass(bodyWeightExercise, " x "+ (10 - counter))
                    exerciseArray.add(ladderObjectClass)

                    counter++
                }


        } catch (e: Exception) {

            e.printStackTrace()
        }



        } else if (MainActivity.unit=="lbs")  {
            // LBS
            if (MainActivity.difficulty == "easy") {

                bodyWeightExercise = ExerciseManager.bodyweightArray[rand.nextInt(bodyweightArray.size - 3)]

            } else {
                bodyWeightExercise = ExerciseManager.bodyweightArray[rand.nextInt(bodyweightArray.size)]
            }

            try {

                val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)
                val exercise = c.getColumnIndex("exercise")
                val weight = c.getColumnIndex("maxLift")

                c.move(rand.nextInt(7))

                if (Integer.parseInt(Integer.parseInt(c.getString(weight)).toString()) == 0) {

                    weightLiftMax = 50
                } else {

                    weightLiftMax = c.getInt(weight)
                }


                val weightLiftExercise = c.getString(exercise)
                var ladderObjectClass: ExerciseObjectClass

                while (counter < 7) {

                    val weightUsed = 5 * (((weightLiftMax * 2.2046226218488 * (0.3 + counter * 0.1) / 5).roundToLong()))

                    ladderObjectClass = ExerciseObjectClass("$weightLiftExercise $weightUsed lbs", " x " + (14 - counter * 2) + "")
                    exerciseArray.add(ladderObjectClass)

                    ladderObjectClass = ExerciseObjectClass(bodyWeightExercise, " x " + (10 - counter))
                    exerciseArray.add(ladderObjectClass)

                    counter++
                }


            } catch (e: Exception) {

                e.printStackTrace()
            }


        }


        return exerciseArray
    }
}