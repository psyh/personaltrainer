package com.alesmandelj.personalizedtrainer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.TextView

class ExerciseInfoClass {

    // this class is used to extract exercise info and send it to ExerciseDemoActivity in order to open exercise demonstration

    lateinit var textViewexercise: String

    internal lateinit var mContext: Context

    fun getmContext(): Context {
        return mContext
    }

    fun setmContext(mContext: Context) {
        this.mContext = mContext
    }

    fun showExercise() {

        val exercise: String

        exercise = if (textViewexercise.contains("x")) {

            val separated = textViewexercise.split(":".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
            separated[0].trim { it <= ' ' }

        } else {

            val separated = textViewexercise.split(":".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
            separated[0].trim { it <= ' ' }
        }

        if (exercise == "Ring muscle up") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "ring_muscle_up")
            mContext.startActivity(intent)

        } else if (exercise == "Pull up") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "pull_up")
            mContext.startActivity(intent)

        } else if (exercise == "Burpee") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "burpee")
            mContext.startActivity(intent)

        } else if (exercise == "Air squat") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "air_squat")
            mContext.startActivity(intent)

        } else if (exercise == "Assault bike") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "assault_bike")
            mContext.startActivity(intent)

        } else if (exercise == "Back squat") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "back_squat")
            mContext.startActivity(intent)

        } else if (exercise == "Bench press") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "bench")
            mContext.startActivity(intent)

        } else if (exercise == "Box jump") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "box_jump")
            mContext.startActivity(intent)

        } else if (exercise == "Clean") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "clean")
            mContext.startActivity(intent)

        } else if (exercise == "Clean and Jerk") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "clean_and_jerk")
            mContext.startActivity(intent)

        } else if (exercise == "Deadlift") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "deadlift")
            mContext.startActivity(intent)

        } else if (exercise == "Double unders") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "double_unders")
            mContext.startActivity(intent)

        } else if (exercise == "Front squat") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "front_squat")
            mContext.startActivity(intent)

        } else if (exercise == "Handstand push up") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "handstand")
            mContext.startActivity(intent)

        } else if (exercise == "Hang Clean") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "hang_clean")
            mContext.startActivity(intent)

        } else if (exercise == "Hang Clean and Jerk") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "hang_clean_and_jerk")
            mContext.startActivity(intent)

        } else if (exercise == "Hang Snatch") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "hang_snatch")
            mContext.startActivity(intent)

        } else if (exercise == "Jump rope") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "jump_rope")
            mContext.startActivity(intent)

        } else if (exercise == "Muscle up") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "muscle_up")
            mContext.startActivity(intent)

        } else if (exercise == "Pistols") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "pistol")
            mContext.startActivity(intent)

        } else if (exercise == "Push up") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "push_up")
            mContext.startActivity(intent)

        } else if (exercise == "Row") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "row")
            mContext.startActivity(intent)

        } else if (exercise == "Ski erg") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "ski_erg")
            mContext.startActivity(intent)

        } else if (exercise == "Snatch") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "snatch")
            mContext.startActivity(intent)

        } else if (exercise == "Sumo Deadlift") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "sumo_deadlift")
            mContext.startActivity(intent)

        } else if (exercise == "Thrusters") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "thruster")
            mContext.startActivity(intent)

        } else if (exercise == "Toes to bar") {

            val intent = Intent(mContext, ExerciseDemoActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("exercise", "toes_to_bar")
            mContext.startActivity(intent)

        }
    }
}
