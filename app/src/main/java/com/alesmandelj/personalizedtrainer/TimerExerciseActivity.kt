package com.alesmandelj.personalizedtrainer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.app.AlertDialog
import android.content.pm.ActivityInfo
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_timer_exercise.*
import kotlinx.android.synthetic.main.activity_timer_exercise.buttonStart
import kotlinx.android.synthetic.main.activity_timer_exercise.buttonStop
import kotlinx.android.synthetic.main.activity_timer_exercise.countTime
import kotlinx.android.synthetic.main.activity_timer_exercise.recycler_View
import kotlinx.android.synthetic.main.activity_timer_exercise.titleTextView

import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timer
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import java.text.SimpleDateFormat


class TimerExerciseActivity : AppCompatActivity(),
        RecyclerItemClickListener.OnRecyclerClickListener {

    private var rand = Random()
    private var exercisesArray= ArrayList<ExerciseObjectClass>()
    var tempExerciseArray =ArrayList<ExerciseObjectClass>()
    var timer=0

    private var mInterstitialAd: InterstitialAd? = null
    internal var addCounter = 0


    fun loadAdd() {

        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.")
        }

    }

    private fun arrayToString(workoutArray:ArrayList<ExerciseObjectClass>):String{

        var wourkoutString=""
        for (elements in workoutArray){

            wourkoutString=wourkoutString+elements.exercise+" ${elements.repetitions} \r\n"
        }
        return wourkoutString
    }

    override fun onBackPressed() {

        if (buttonStart.text.toString() === "Stop") {

            AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit workout?")
                    .setMessage("Are you sure you want to exit current workout?")
                    .setPositiveButton("Yes") { dialog, which ->
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        android.os.Process.killProcess(android.os.Process.myPid())
                    }
                    .setNegativeButton("No") { dialog, which -> }
                    .show()
        } else {

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()

        }
    }

    private fun createEmom():ArrayList<ExerciseObjectClass>{
        var count=0
        timer = rand.nextInt(14 - 7) + 7
        titleTextView.text="Do all exercises every minute for: $timer min"

        exercisesArray.clear()
        tempExerciseArray.clear()

        tempExerciseArray = ExerciseManager.generateTraining(1, 4, 0.75)

        while(count<3){
            exercisesArray.add(tempExerciseArray[count])
            count++
        }
        countTime.text = "$timer:00"
        return exercisesArray
    }


    private fun createAmrap():ArrayList<ExerciseObjectClass>{

        val howMany = rand.nextInt(5 - 3) + 3        //random 3 or 4 exercise workout
        var count=0
        timer = rand.nextInt(31 - 18) + 18     //set time and timer
        titleTextView.text="Do as many rounds in: $timer min"
        exercisesArray.clear()
        tempExerciseArray.clear()

        tempExerciseArray = ExerciseManager.generateTraining(3, 8, 0.6)

        while(count<howMany){
            exercisesArray.add(tempExerciseArray[count])
            count++
        }
        countTime.text = "$timer:00"
        return exercisesArray
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer_exercise)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        //actionbar
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "New Activity"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        //ADDS

        MobileAds.initialize(this) { }

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd!!.adUnitId = "ca-app-pub-9141584298272884/3797282850"

        mInterstitialAd!!.loadAd(AdRequest.Builder().build())


        buttonStop.text = "New"
        buttonStart.text = "Start"
        val selectedWorkout = intent.getStringExtra("workout")

        //play add
        if (selectedWorkout == "emom") {
            createEmom()
            actionbar!!.title = "EMOM"
        }
        else if (selectedWorkout == "amrap") {
            createAmrap()
            actionbar!!.title = "AMRAP"
        }


        val recyclerViewAdapter = RecyclerAdapter(exercisesArray)
        recycler_View.layoutManager = LinearLayoutManager(this)
        recycler_View.addOnItemTouchListener(RecyclerItemClickListener(this,recycler_View,this))
        recycler_View.adapter = recyclerViewAdapter


        buttonStop.setOnClickListener {
            if (buttonStop.text == "New") {
                if (selectedWorkout == "emom") {
                    createEmom()
                } else if (selectedWorkout == "amrap") {
                    createAmrap()
                }
                recycler_View.adapter = recyclerViewAdapter
                addCounter++


                //play add

                if (addCounter == 12) {

                    loadAdd()
                    addCounter = 0
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())

                }
            }
        }


        buttonStart.setOnClickListener {
            if (buttonStart.text.toString() == "Start") {
                val date = SimpleDateFormat("dd.MMM.yyyy", Locale.getDefault()).format(Date())
                var workoutSaveString = arrayToString(exercisesArray)

                val history = date + "\r\n" + selectedWorkout.toUpperCase() +"  "+ timer + " min \r\n" + workoutSaveString
                try {

                    MainActivity.maxDB.execSQL("INSERT INTO workoutHistory(workout) VALUES ('$history')")

                } catch (e: Exception) {

                    e.printStackTrace()
                }
            }
            TimerClass.goFunction(countTime, buttonStart, applicationContext, "$timer", buttonStop, this)
        }

    }

    override fun onItemClick(view: View, position: Int) {
        Log.d("ItemClick:","yes")
        val exe1 = ExerciseInfoClass()
        exe1.textViewexercise = exercisesArray[position].exercise
        exe1.mContext = applicationContext
        exe1.showExercise()

    }

    override fun onItemLongClick(view: View, position: Int) {
        Log.d("ItemClick:","Long")
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
