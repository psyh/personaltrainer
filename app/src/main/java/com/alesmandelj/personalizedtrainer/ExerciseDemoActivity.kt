package com.alesmandelj.personalizedtrainer

import android.content.Intent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.webkit.WebView

class ExerciseDemoActivity : AppCompatActivity() {

    private lateinit var webView: WebView
    private lateinit var intentString: String
    private lateinit var layout: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise_demo)

        layout = findViewById(R.id.layout)
        webView = findViewById(R.id.webView)

        val intent = intent

        intentString = intent.getStringExtra("exercise")

        webView.loadUrl("file:///android_asset/$intentString.gif")

        layout.setOnClickListener { finish() }

        webView.setOnTouchListener { v, event ->
            finish()
            false
        }
    }
}
