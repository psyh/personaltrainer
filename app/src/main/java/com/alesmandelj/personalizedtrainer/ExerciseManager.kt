package com.alesmandelj.personalizedtrainer

import java.util.*


object ExerciseManager {                                                                       //Combining bodyweight exercises and weighted lifts into a single array and add repetitions

    // Get Exerises

    val bodyweightArray = ArrayList(Arrays.asList("Pull up: ", "Burpee: ", "Push up: ", "Toes to bar: ", "Air squat: ", "Box jump: ", "Pistols: ", "Muscle up: ", "Ring muscle up: ", "Handstand push up: "))

    // Generate

    fun generateTraining(low: Int, high: Int, weightModifier: Double): ArrayList<ExerciseObjectClass> {

        val exercisesArray = ArrayList<ExerciseObjectClass>()
        var getExercise: String


        val rand = Random()

        //body weight exercises
        if (MainActivity.difficulty == "easy") {      //if easy using 4 less exercises and repetitions +2
            for (i in 0 until bodyweightArray.size - 4) {

                val repsBody = rand.nextInt(high - low) + low   //high low come from exercise acitivity

                val exe1 = ExerciseObjectClass(bodyweightArray[i], "  x " + (repsBody + 2))
                exercisesArray.add(exe1)
            }
        } else if (MainActivity.difficulty == "advanced") {       // if hard ->easy exercises are *2 and added four new

            for (i in bodyweightArray.indices) {

                val repsBody = rand.nextInt(high - low) + low        //high low come from exercise acitivity

                if (i < bodyweightArray.size - 4 && high != 4) {

                    val exe1 = ExerciseObjectClass(bodyweightArray[i], "  x " + (repsBody + 2))

                    exercisesArray.add(exe1)

                } else {

                    val exe1 = ExerciseObjectClass(bodyweightArray[i], "  x " + (repsBody + 2))

                    exercisesArray.add(exe1)
                }
            }
        }

        //weightlifting exercises

        if (MainActivity.unit == "kg") {

            try {

                val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)   //go through exercise database and add repetitions

                val exercise = c.getColumnIndex("exercise")
                val weight = c.getColumnIndex("maxLift")
                var count = 0

                c.moveToFirst()

                while (count < 7) {

                    if (Integer.parseInt(Integer.parseInt(c.getString(weight)).toString()) == 0) {
                        c.moveToNext()
                    } else {

                        val newLift = 5 * Math.round(Integer.parseInt(c.getString(weight)) * weightModifier / 5)
                        val repsRandom = rand.nextInt(high - low) + low

                        //if certain exercises add hang

                        if (c.getString(exercise) == "Clean and Jerk: ") {

                            getExercise = "Thrusters: "

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)
                        } else if (c.getString(exercise) == "Clean: " || c.getString(exercise) == "Snatch: ") {

                            getExercise = "Hang " + c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)

                            getExercise = c.getString(exercise)

                            val exe2 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe2)

                        } else if (c.getString(exercise) == "Deadlift: ") {          //make two wersions of deadlift

                            getExercise = "Sumo " + c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)

                            getExercise = c.getString(exercise)

                            val exe2 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe2)

                        } else {

                            getExercise = c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)
                        }

                        count++
                        c.moveToNext()

                    }
                }
                Collections.shuffle(exercisesArray)     //shuffle array so we get random exercises order

            } catch (e: Exception) {

                e.printStackTrace()
            }

        } else if (MainActivity.unit == "lbs") {  // LBS

            try {

                val c = MainActivity.maxDB.rawQuery("SELECT * FROM myMax ", null)   //go through exercise database and add repetitions

                val exercise = c.getColumnIndex("exercise")
                val weight = c.getColumnIndex("maxLift")
                var count = 0

                c.moveToFirst()

                while (count < 7) {
                    if (Integer.parseInt(Integer.parseInt(c.getString(weight)).toString()) == 0) {
                        c.moveToNext()
                    } else {

                        val newLift = 5 * Math.round(Integer.parseInt(c.getString(weight)).toDouble() * 2.2046226218488 * weightModifier / 5)
                        val repsRandom = rand.nextInt(high - low) + low

                        //if certain exercises add hang

                        if (c.getString(exercise) == "Clean and Jerk: ") {

                            getExercise = "Thrusters: "

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)
                        } else if (c.getString(exercise) == "Clean: " || c.getString(exercise) == "Snatch: ") {


                            getExercise = "Hang " + c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)

                            getExercise = c.getString(exercise)

                            val exe2 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe2)

                        } else if (c.getString(exercise) == "Deadlift: ") {          //make two wersions of deadlift


                            getExercise = "Sumo " + c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)

                            getExercise = c.getString(exercise)

                            val exe2 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe2)

                        } else {

                            getExercise = c.getString(exercise)

                            val exe1 = ExerciseObjectClass(getExercise, newLift.toString() + " " + MainActivity.unit + " x " + repsRandom)

                            exercisesArray.add(exe1)
                        }

                        count++
                        c.moveToNext()

                    }
                }
                Collections.shuffle(exercisesArray)     //shuffle array so we get random exercises order

            } catch (e: Exception) {

                e.printStackTrace()
            }

        }
        Collections.shuffle(exercisesArray)
        return exercisesArray
    }
}
