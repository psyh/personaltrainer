package com.alesmandelj.personalizedtrainer

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

private const val TAG="RecyclerAdapter"

class ExerciseMaxViewHolder(view :View) :RecyclerView.ViewHolder(view){
    var repetitions: TextView =view.findViewById(R.id.max_lift_Text_View)
    var exercise : TextView =view.findViewById(R.id.name_max_TextView)
}

class ListMaxAdapter(private val listOfMaxExercises:ArrayList<ExerciseObjectClass>):RecyclerView.Adapter<ExerciseMaxViewHolder>() {
    override fun onBindViewHolder(holder: ExerciseMaxViewHolder, position: Int) {
        if(listOfMaxExercises.isEmpty()){
            holder.repetitions.text=("")
            holder.exercise.text="No exercises found"
        }else{
            val exerciseItem=listOfMaxExercises[position]
            holder.repetitions.text=exerciseItem.repetitions
            holder.exercise.text=exerciseItem.exercise
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseMaxViewHolder {
        Log.d(TAG,".onCreateViewHolder new view requested")
        val view= LayoutInflater.from(parent.context).inflate(R.layout.exercise_max_details,parent,false)
        return ExerciseMaxViewHolder(view)
    }

    override fun getItemCount(): Int {
        //  Log.d(TAG,".getItemCount called")
        return if (listOfMaxExercises.isNotEmpty()) listOfMaxExercises.size else 0
    }



}