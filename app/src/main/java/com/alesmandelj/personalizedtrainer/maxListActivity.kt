package com.alesmandelj.personalizedtrainer

import android.content.Intent
import android.content.pm.ActivityInfo
import android.media.AudioManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_max_list.*
import java.util.ArrayList


class maxListActivity : AppCompatActivity() {



    override fun onBackPressed() {

        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_max_list)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        volumeControlStream = AudioManager.STREAM_MUSIC


        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Workout history"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        maxArray = ArrayList()

        arrayAdapter2 = ArrayAdapter<String>(this,R.layout.exercise_detail_other, maxArray)

        maxListView.adapter = arrayAdapter2

        val getMyMax = intent
        val selected = getMyMax.getIntExtra("exercise", 0)

        if (selected == 0) {       //if certain variable is selected fill ListView with specific exercise

            updateListView("bench")
            actionbar!!.title = "Bench press:"

        } else if (selected == 1) {

            updateListView("deadlift")
            actionbar!!.title = "Deadlift:"

        } else if (selected == 2) {

            updateListView("fSquat")
            actionbar!!.title = "Front squat:"

        } else if (selected == 3) {

            updateListView("bSquat")
            actionbar!!.title = "Back squat:"

        } else if (selected == 4) {

            updateListView("clean")
            actionbar!!.title = "Clean"

        } else if (selected == 5) {

            updateListView("cleanJerk")
            actionbar!!.title = "Clean and jerk:"

        } else if (selected == 6) {

            updateListView("snatch")
            actionbar!!.title = "Snatch:"

        }
    }

    companion object {
        internal lateinit var maxArray: ArrayList<String>
        internal lateinit var arrayAdapter2: ArrayAdapter<*>

        fun updateListView(selection: String) {       //update listVIew with maximum of specific exercise

            maxArray.clear()

            try {

                val c2 = MainActivity.maxDB.rawQuery("SELECT * FROM $selection", null)
                val contentIndex = c2!!.getColumnIndex("maxLift")
                val titleIndex = c2.getColumnIndex("datum")

                if (MainActivity.unit == "lbs") {
                    c2.moveToFirst()

                    while (c2 != null) {

                        val maxLift = c2.getString(contentIndex)
                        var asdf = java.lang.Double.valueOf(maxLift)
                        asdf = asdf * 2.2046226218488
                        asdf = (Math.round(asdf / 5) * 5).toDouble()
                        val maxLiftInt = asdf.toInt()
                        maxArray.add(maxLiftInt.toString() + " " + MainActivity.unit + "    " + c2.getString(titleIndex))
                        c2.moveToNext()
                    }

                } else if (MainActivity.unit == "kg") {
                    c2.moveToFirst()

                    while (c2 != null) {
                        val maxLift = c2.getString(contentIndex)
                        var asdf = java.lang.Double.valueOf(maxLift)
                        asdf = Math.round(asdf).toDouble()
                        val maxLiftInt = asdf.toInt()
                        maxArray.add(maxLiftInt.toString() + " " + MainActivity.unit + "    " + c2.getString(titleIndex))
                        c2.moveToNext()
                    }
                }


            } catch (e: Exception) {

                e.printStackTrace()
            }

            maxArray.reverse()

            arrayAdapter2.notifyDataSetChanged()
        }
    }
}
