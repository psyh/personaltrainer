package com.alesmandelj.personalizedtrainer


import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.database.sqlite.SQLiteDatabase
import android.media.AudioManager
import android.os.Bundle

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.content_main.*

import java.util.ArrayList
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        RecyclerItemClickListener.OnRecyclerClickListener {

    private var mInterstitialAd: InterstitialAd? = null
 //   private var exerciseArray= ArrayList<ExerciseObjectClass>()
    //Update listView with maximum lifts

    fun loadAdd() {

        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.")
        }
    }

    fun updateMenuItem(){

        if (unit=="kg"){
            R.id.grooup_unit
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        volumeControlStream = AudioManager.STREAM_MUSIC
        title = "Your max lifts:"
        setTheme(R.style.AppTheme_NoActionBar)

        //ADDS

        MobileAds.initialize(this) { }

        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd!!.adUnitId = "ca-app-pub-9141584298272884/3797282850"
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())

        //Get unit and dificulty from settings

        val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
        unit = sharedPreferences.getString("unit", "kg")
        difficulty = sharedPreferences.getString("difficulty", "easy")

        updateMenuItem()

        setSupportActionBar(toolbar)

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)





        //Cardio array

        cardioArray.clear()
        cardioArray.add("Jump rope: ")
        cardioArray.add("Double unders: ")
        cardioArray.add("Run: ")
        cardioArray.add("Ski erg: ")
        cardioArray.add("Row: ")
        cardioArray.add("Assault bike: ")

        // Make database new max

        maxDB = this.openOrCreateDatabase("Workouts", Context.MODE_PRIVATE, null)
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS myMax (id INTEGER PRIMArY KEY, exercise STRING, maxLift INT)")

        // ex max

        maxDB.execSQL("CREATE TABLE IF NOT EXISTS exMax(id INTEGER PRIMARY KEY, bench STRING, deadlift STRING, fSquat STRING, bSquat STRING, clean STRING, cleandJerk STRING, snatch STRING, pullUp STRING)")

        maxDB.execSQL("CREATE TABLE IF NOT EXISTS bench (id INTEGER PRIMArY KEY, maxLift INT, datum STRING )")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS deadlift (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS fSquat (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS bSquat (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS clean (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS cleanJerk (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")
        maxDB.execSQL("CREATE TABLE IF NOT EXISTS snatch (id INTEGER PRIMArY KEY, maxLift INT, datum STRING)")

        //workout history

        maxDB.execSQL("CREATE TABLE IF NOT EXISTS workoutHistory(id INTEGER PRIMARY KEY, workout STRING)")

        exerciseArray= updateListView()

        val arrayMaxAdapter = ListMaxAdapter(exerciseArray)
        MaxLiftListView.layoutManager = LinearLayoutManager(this)
        MaxLiftListView.addOnItemTouchListener(RecyclerItemClickListener(this,MaxLiftListView,this))
        MaxLiftListView.adapter = arrayMaxAdapter


    }

    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)


        if (unit=="kg"){

            menu.findItem(R.id.unit_kg).isChecked = true
        }else{
            menu.findItem(R.id.unit_pounds).isChecked=true
        }

        if(difficulty=="easy"){
            menu.findItem(R.id.difficulty_easy).isChecked=true

        }else{
            menu.findItem(R.id.difficulty_advanced).isChecked=true

        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId


        if (id == R.id.set_max) {

            val intent = Intent(applicationContext, myMaxActivity::class.java)
            startActivity(intent)
            finish()
            return true

        }else if (id==R.id.difficulty_easy){
            difficulty="easy"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("difficulty", difficulty).apply()
            item.isChecked=true
        }else if (id==R.id.difficulty_advanced){
            difficulty="advanced"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("difficulty", difficulty).apply()
            item.isChecked = true

        }else if (id==R.id.unit_pounds){
            unit="lbs"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("unit", unit).apply()
            item.isChecked=true
            updateListView()
        }else if (id==R.id.unit_kg){
            unit="kg"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("unit", unit).apply()
            item.isChecked=true
            updateListView()
        }
        val arrayMaxAdapter = ListMaxAdapter(exerciseArray)
        MaxLiftListView.layoutManager = LinearLayoutManager(this)
        MaxLiftListView.addOnItemTouchListener(RecyclerItemClickListener(this,MaxLiftListView,this))
        MaxLiftListView.adapter = arrayMaxAdapter

        return super.onOptionsItemSelected(item)
    }

    //Side menu
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.workout) {

            val intent = Intent(applicationContext, TimerExerciseActivity::class.java)
            intent.putExtra("workout","emom")
            startActivity(intent)
            finish()
            loadAdd()

        } else if (id == R.id.Amrap) {

            val intent = Intent(applicationContext, TimerExerciseActivity::class.java)
            intent.putExtra("workout","amrap")
            startActivity(intent)
            finish()
            loadAdd()

        } else if (id == R.id.Ladder) {

            val intent = Intent(applicationContext, StopwatchExerciseActivity::class.java)
            intent.putExtra("workout","ladder")
                startActivity(intent)
                finish()
                loadAdd()

        } else if (id == R.id.Cardio) {

                val intent = Intent(applicationContext, StopwatchExerciseActivity::class.java)
                intent.putExtra("workout","cardio")
                startActivity(intent)
                finish()
                loadAdd()

        } else if (id == R.id.FullCardio) {

                val intent = Intent(applicationContext, StopwatchExerciseActivity::class.java)
                intent.putExtra("workout","fullCardio")
                startActivity(intent)
                finish()
                loadAdd()

        } else if (id == R.id.history) {

            val intent = Intent(applicationContext, workoutHistory::class.java)
            startActivity(intent)
            finish()

        } else if (id == R.id.Help) {

            val intent = Intent(applicationContext, HelpActivity::class.java)
            startActivity(intent)
            finish()
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    companion object {

        //Variables

        internal lateinit var maxDB: SQLiteDatabase
        internal var exerciseArray = ArrayList<ExerciseObjectClass>()
        internal var unit: String? = null
        internal var difficulty: String? = null
        internal var cardioArray = ArrayList<String>()
        internal lateinit var arrayAdapter: ArrayAdapter<String>
        internal var addString = "\n" + "ca-app-pub-9141584298272884/3797282850"

        fun updateListView():ArrayList<ExerciseObjectClass> {

            val c = maxDB.rawQuery("SELECT * FROM myMax", null)

            exerciseArray.clear()


            try {


                val maxIndex = c!!.getColumnIndex("maxLift")
                val exerciseIndex = c.getColumnIndex("exercise")

                if (unit == "lbs") {
                    c.moveToFirst()

                    while (c != null) {
                        if (Integer.parseInt(Integer.parseInt(c.getString(maxIndex)).toString()) == 0) {
                            c.moveToNext()
                        } else {
                            val maxLift = 5 * ((Integer.parseInt(c.getString(maxIndex)) * 2.2046226218488 / 5).roundToInt())
                            var exerciseObject=ExerciseObjectClass("","")
                            exerciseObject.exercise=c.getString(exerciseIndex)
                            exerciseObject.repetitions= "$maxLift $unit"
                            exerciseArray.add(exerciseObject)
                            c.moveToNext()
                        }
                    }
                } else {
                    c.moveToFirst()

                    while (c != null) {
                        if (Integer.parseInt(Integer.parseInt(c.getString(maxIndex)).toString()) == 0) {
                            c.moveToNext()
                        } else {
                            val maxLift =  Integer.parseInt(c.getString(maxIndex))
                            var exerciseObject=ExerciseObjectClass("","")
                            exerciseObject.exercise=c.getString(exerciseIndex)
                            exerciseObject.repetitions= "$maxLift $unit"
                            exerciseArray.add(exerciseObject)
                            c.moveToNext()
                        }
                    }
                }



            } catch (e: Exception) {

                e.printStackTrace()
            }

            if (exerciseArray.isEmpty()) {

                var exerciseObjectEmptyOne=ExerciseObjectClass("Click here to set your max or click top right icon to access Settings","")
                var exerciseObjectEmptyTwo=ExerciseObjectClass("Click top left icon to access workouts","")
                var exerciseObjectEmptyThree=ExerciseObjectClass("Tap exercise name to display exercise demonstration","")
                exerciseArray.add(exerciseObjectEmptyOne)
                exerciseArray.add(exerciseObjectEmptyTwo)
                exerciseArray.add(exerciseObjectEmptyThree)
            }
            return exerciseArray
        }
    }

    override fun onItemClick(view: View, position: Int) {
        Log.d("Clicked","yes")
        if (exerciseArray[position].exercise === "Click here to set your max or click top right icon to access Settings") {

            val newInten = Intent(applicationContext, myMaxActivity::class.java)
            startActivity(newInten)
            finish()
        } else if (exerciseArray[position].exercise === "Click top left icon to access workouts") {
        } else {

            val getMyMax = Intent(applicationContext, maxListActivity::class.java)
            getMyMax.putExtra("exercise", position)
            startActivity(getMyMax)
            finish()
        }

    }

    override fun onItemLongClick(view: View, position: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}


