package com.alesmandelj.personalizedtrainer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import java.util.ArrayList

class workoutHistory : AppCompatActivity() {

    internal lateinit var historyListView: ListView

    override fun onBackPressed() {

        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workout_history)

        historyListView = findViewById(R.id.historyListView)

        savedArrayAdapter = ArrayAdapter<String>(this,R.layout.exercise_detail_other, savedWorkouts)

        historyListView.adapter = savedArrayAdapter
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Workout history"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)
        updateListView()
    }

    companion object {
        internal var savedWorkouts = ArrayList<String>()
        internal lateinit var savedArrayAdapter: ArrayAdapter<*>

        fun updateListView() {

            val c = MainActivity.maxDB.rawQuery("SELECT * FROM workoutHistory", null)

            savedWorkouts.clear()

            try {

                val exerciseIndex = c!!.getColumnIndex("workout")

                c.moveToFirst()

                while (c != null) {

                    savedWorkouts.add(c.getString(exerciseIndex))
                    c.moveToNext()

                }
            } catch (e: Exception) {

                e.printStackTrace()

            }

            savedWorkouts.reverse()
            savedArrayAdapter.notifyDataSetChanged()
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
