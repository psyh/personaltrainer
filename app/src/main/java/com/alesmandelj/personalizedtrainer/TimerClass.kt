package com.alesmandelj.personalizedtrainer

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.media.MediaPlayer
import android.os.CountDownTimer
import android.view.View
import android.widget.Button
import android.widget.TextView

object TimerClass {

    internal lateinit var timerStart: String
    internal var time: Long? = null
    internal lateinit var mPlayer: MediaPlayer
    internal var counterIsActive: Boolean? = false
    internal lateinit var countDownTimer: CountDownTimer

    fun updateTimer(secondsLeft: Int, textView: TextView) {

        val minutes = secondsLeft / 60
        val seconds = secondsLeft - minutes * 60

        var secondString = Integer.toString(seconds)

        if (seconds <= 9) {

            secondString = "0$secondString"
        }

        textView.text = Integer.toString(minutes) + ":" + secondString
    }

    fun goFunction(textView: TextView, button: Button, mContext: Context, doForTime: String, generateNew: Button, sContext: Context) {

        if (counterIsActive == false) {

            counterIsActive = true
            generateNew.visibility = View.INVISIBLE
            button.text = "Stop"
            timerStart = textView.toString()

            countDownTimer = object : CountDownTimer((Integer.parseInt(doForTime) * 60000 + 1000).toLong(), 100) {
                var counter = 0
                override fun onTick(millisUntilFinished: Long) {

                    updateTimer(millisUntilFinished.toInt() / 1000, textView)
                    counter++

                    if (counter == 600) {

                        mPlayer = MediaPlayer.create(mContext, R.raw.boom)
                        mPlayer.start()

                        counter = 0
                    }
                }

                override fun onFinish() {

                    textView.text = "0:00"
                    mPlayer = MediaPlayer.create(mContext, R.raw.bloop)
                    mPlayer.start()
                    textView.text = "Done"
                    countDownTimer.cancel()
                    button.text = "Start"

                    generateNew.visibility = View.VISIBLE
                    counterIsActive = false
                }
            }.start()
        } else {

            AlertDialog.Builder(sContext, AlertDialog.THEME_HOLO_DARK)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Stop current workout?")
                    .setMessage("Are you sure you want to stop current workout?")
                    .setPositiveButton("Yes") { dialog, which ->
                        textView.text = doForTime + ":00"
                        generateNew.visibility = View.VISIBLE
                        countDownTimer.cancel()
                        button.text = "Start"
                        counterIsActive = false
                    }
                    .setNegativeButton("No") { dialog, which -> }
                    .show()
        }
    }
}
