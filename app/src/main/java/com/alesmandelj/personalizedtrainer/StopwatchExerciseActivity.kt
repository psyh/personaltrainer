package com.alesmandelj.personalizedtrainer

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_stopwatch_exercise.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class StopwatchExerciseActivity : AppCompatActivity(),
        RecyclerItemClickListener.OnRecyclerClickListener {

    private var rand = Random()
    var exercisesArray= ArrayList<ExerciseObjectClass>()
    var tempExerciseArray =ArrayList<ExerciseObjectClass>()
    internal var index: Int = 0
    internal var weightLiftMax: Int = 0

    private var mInterstitialAd: InterstitialAd? = null
    private var addCounter = 0


    fun loadAdd() {

        if (mInterstitialAd!!.isLoaded) {
            mInterstitialAd!!.show()
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.")
        }

    }

    private fun arrayToString(workoutArray:ArrayList<ExerciseObjectClass>):String{

        var wourkoutString=""
        for (elements in workoutArray){

            wourkoutString=wourkoutString+elements.exercise+" ${elements.repetitions} \r\n"
        }
        return wourkoutString
    }

    override fun onBackPressed() {

        if (buttonStart.text.toString() === "Stop") {

            AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Exit workout?")
                    .setMessage("Are you sure you want to exit current workout?")
                    .setPositiveButton("Yes") { dialog, which ->
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        android.os.Process.killProcess(android.os.Process.myPid())
                    }
                    .setNegativeButton("No") { dialog, which -> }
                    .show()
        } else {

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()

        }
    }

    private fun createCardio(): ArrayList<ExerciseObjectClass> {

        titleTextView.text="Rounds to do: " + (rand.nextInt(11 - 7) + 7).toString()
        val howMany = rand.nextInt(5 - 3) + 3
        var count=0
        exercisesArray.clear()
        tempExerciseArray.clear()


        tempExerciseArray = ExerciseManager.generateTraining(3, 8, 0.65)

        while(count<howMany){
            exercisesArray.add(tempExerciseArray[count])
            count++
        }

        val randCardioExercise = rand.nextInt(6)     //select cardio exercise and add repetitions or calories

        var cardioExerciseObject: ExerciseObjectClass

        cardioExerciseObject = if (randCardioExercise == 0 || randCardioExercise == 1) {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],((rand.nextInt(11 - 5) + 5) * 10).toString())
        } else if (randCardioExercise == 2) {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],((rand.nextInt(5 - 1) + 1) * 100).toString() + " m")
        } else {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],(rand.nextInt(21 - 10) + 9).toString() + " cal")
        }
        exercisesArray.add(cardioExerciseObject)

        return exercisesArray
    }

    private fun createFullCardio():ArrayList<ExerciseObjectClass>{

        titleTextView.text="Rounds to do: " + (rand.nextInt(11 - 7) + 7).toString()

        var count=0
        exercisesArray.clear()
        tempExerciseArray.clear()

        val randCardioExercise = rand.nextInt(6)     //select cardio exercise and add repetitions or calories

        var cardioExerciseObject: ExerciseObjectClass

        cardioExerciseObject = if (randCardioExercise == 0 || randCardioExercise == 1) {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],((rand.nextInt(11 - 5) + 5) * 10).toString())
        } else if (randCardioExercise == 2) {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],((rand.nextInt(5 - 1) + 1) * 100).toString() + " m")
        } else {
            ExerciseObjectClass(MainActivity.cardioArray[randCardioExercise],(rand.nextInt(21 - 10) + 9).toString() + " cal")
        }
        tempExerciseArray = ExerciseManager.generateTraining(4, 9, 0.7)

        while(count<3){
            exercisesArray.add(tempExerciseArray[count])
            exercisesArray.add(cardioExerciseObject)
            count++
        }

        return exercisesArray
    }

    private fun createLadder():ArrayList<ExerciseObjectClass>{

        titleTextView.text="Take 90 seconds rest between sets"
        exercisesArray.clear()

        exercisesArray=LadderManager.generateLadder()

       return exercisesArray
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stopwatch_exercise)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        //actionbar
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "New Activity"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        buttonStop.text="New"
        buttonStart.text="Start"

        val stopwatch = StopWatchClass()
        stopwatch.textMinutes = countTime
        stopwatch.textMilliseconds = textViewMilliseconds
        stopwatch.textHours = textViewHours
        stopwatch.startButton = buttonStart
        stopwatch.stopButton = buttonStop

        MobileAds.initialize(this) { }
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd!!.adUnitId = "ca-app-pub-9141584298272884/3797282850"
        mInterstitialAd!!.loadAd(AdRequest.Builder().build())

        val selectedWorkout=intent.getStringExtra("workout")



        when (selectedWorkout) {
            "cardio" -> {createCardio()
                actionbar.title = "Cardio"
            }
            "fullCardio" -> {createFullCardio()
                actionbar.title = "Full Cardio"
            }
            "ladder" -> {createLadder()
                actionbar.title = "Ladder"}
        }

        var recyclerViewAdapter=RecyclerAdapter(exercisesArray)
        recycler_View.addOnItemTouchListener(RecyclerItemClickListener(this,recycler_View,this))
        recycler_View.layoutManager= LinearLayoutManager(this)
        recycler_View.adapter=recyclerViewAdapter



        buttonStop.setOnClickListener {
            if (buttonStop.text == "New"){
                when (selectedWorkout) {
                    "cardio" -> createCardio()
                    "fullCardio" -> createFullCardio()
                    "ladder" -> createLadder()
                }

                recyclerViewAdapter=RecyclerAdapter(exercisesArray)

                recycler_View.layoutManager= LinearLayoutManager(this)
               // recycler_View.addOnItemTouchListener(RecyclerItemClickListener(this,recycler_View,this))
                recycler_View.adapter=recyclerViewAdapter
                for (elements in exercisesArray){
                    Log.d("elements:", "${elements.exercise}")
                }
                //play add
                addCounter++
                if (addCounter == 12) {

                    loadAdd()
                    addCounter = 0
                    mInterstitialAd!!.loadAd(AdRequest.Builder().build())
                }
            }



            else if (buttonStop.text == "Stop") {stopwatch.stopFunction()}
            else if (buttonStop.text == "Reset") {
                stopwatch.stopFunction()
                buttonStop.text = "New"
            }

        }

        buttonStart.setOnClickListener {
            if (buttonStart.text === "Start") {
                val date = SimpleDateFormat("dd.MMM.yyyy", Locale.getDefault()).format(Date())
                var workoutSaveString= arrayToString(exercisesArray)
                val history = date + "\r\n" +selectedWorkout.toUpperCase() + "\r\n"        +workoutSaveString
                try {

                    MainActivity.maxDB.execSQL("INSERT INTO workoutHistory(workout) VALUES ('$history')")

                } catch (e: Exception) {

                    e.printStackTrace()

                }
                buttonStop.text = "Stop"

            }
            stopwatch.startFunction()
        }





    }

    override fun onItemClick(view: View, position: Int) {
        Log.d("ItemClick:","yes")
        val exe1 = ExerciseInfoClass()
        exe1.textViewexercise = exercisesArray[position].exercise
        exe1.mContext = applicationContext
        exe1.showExercise()

    }

    override fun onItemLongClick(view: View, position: Int) {
        Log.d("ItemClick:","Long")
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
