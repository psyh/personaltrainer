package com.alesmandelj.personalizedtrainer

import androidx.appcompat.app.AppCompatActivity

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.content_main.*

class HelpActivity : AppCompatActivity() {


    override fun onBackPressed() {       // redefine back button action

        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        //actionbar
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Help"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        if (MainActivity.unit =="kg"){

            menu.findItem(R.id.unit_kg).isChecked = true
        }else{
            menu.findItem(R.id.unit_pounds).isChecked=true
        }

        if(MainActivity.difficulty =="easy"){
            menu.findItem(R.id.difficulty_easy).isChecked=true

        }else{
            menu.findItem(R.id.difficulty_advanced).isChecked=true

        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId


        if (id == R.id.set_max) {

            val intent = Intent(applicationContext, myMaxActivity::class.java)
            startActivity(intent)
            finish()
            return true

        }else if (id==R.id.difficulty_easy){
            MainActivity.difficulty ="easy"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("difficulty", MainActivity.difficulty).apply()
            item.isChecked=true
        }else if (id==R.id.difficulty_advanced){
            MainActivity.difficulty ="advanced"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("difficulty", MainActivity.difficulty).apply()
            item.isChecked = true

        }else if (id==R.id.unit_pounds){
            MainActivity.unit ="lbs"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("unit", MainActivity.unit).apply()
            item.isChecked=true
            MainActivity.updateListView()
        }else if (id==R.id.unit_kg){
            MainActivity.unit ="kg"
            val sharedPreferences = this.getSharedPreferences("com.alesmandelj.personalizedtrainer", Context.MODE_PRIVATE)
            sharedPreferences.edit().putString("unit", MainActivity.unit).apply()
            item.isChecked=true
            MainActivity.updateListView()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
