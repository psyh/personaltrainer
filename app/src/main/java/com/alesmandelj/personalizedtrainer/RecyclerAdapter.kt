package com.alesmandelj.personalizedtrainer

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

private const val TAG="RecyclerAdapter"
class ExerciseViewHolder(view :View) :RecyclerView.ViewHolder(view){
    var repetitions: TextView =view.findViewById(R.id.textViewReps)
    var exercise : TextView =view.findViewById(R.id.textViewExercise)
}

class RecyclerAdapter(private val listOfExercises:ArrayList<ExerciseObjectClass>):RecyclerView.Adapter<ExerciseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        Log.d(TAG,".onCreateViewHolder new view requested")
        val view= LayoutInflater.from(parent.context).inflate(R.layout.exercise_details,parent,false)
        return ExerciseViewHolder(view)
    }

    override fun getItemCount(): Int {
        //  Log.d(TAG,".getItemCount called")
        return if (listOfExercises.isNotEmpty()) listOfExercises.size else 0
    }

    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        if(listOfExercises.isEmpty()){
            holder.repetitions.text=("")
            holder.exercise.text="No exercises found"
        }else{
           val exerciseItem=listOfExercises[position]
            holder.repetitions.text=exerciseItem.repetitions
            holder.exercise.text=exerciseItem.exercise
        }
    }

}
