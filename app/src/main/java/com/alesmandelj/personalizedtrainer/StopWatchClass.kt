package com.alesmandelj.personalizedtrainer

import android.os.Handler
import android.os.SystemClock
import android.view.View
import android.widget.Button
import android.widget.TextView

class StopWatchClass {

    internal var timerRuns = false
    internal var stopped = false
    internal var minutes: Int = 0
    internal var seconds: Int = 0
    internal var miliSeconds: Int = 0
    internal var hours: Int = 0
    internal var handler = Handler()

    internal var startTime: Long = 0
    internal var milisecondsTime: Long = 0
    internal var currentTime: Long = 0
    internal var newTime = 0L

    lateinit var stopButton: Button
    lateinit var startButton: Button
    lateinit var textMinutes: TextView
    lateinit var textMilliseconds: TextView
    lateinit var textHours: TextView

    val runnable: Runnable = object : Runnable {
        override fun run() {

            milisecondsTime = SystemClock.uptimeMillis() - startTime
            newTime = currentTime + milisecondsTime
            seconds = (newTime / 1000).toInt()
            hours = seconds / 3600
            minutes = seconds / 60 - hours * 60
            seconds = seconds % 60
            miliSeconds = (newTime % 1000 / 10).toInt()

            textMinutes.setText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds))
            textMilliseconds.text = ":" + String.format("%02d", miliSeconds)
            textHours.setText(String.format("%02d", hours) + ":")

            handler.postDelayed(this, 0)
        }
    }

    fun startFunction() {

        if (timerRuns == false) {
            startTime = SystemClock.uptimeMillis()
            handler.postDelayed(runnable, 0)
            timerRuns = true
            startButton.text = "Pause"
        } else {

            currentTime += milisecondsTime
            timerRuns = false
            startButton.text = "Resume"
            handler.removeCallbacks(runnable)
        }
    }

    fun stopFunction() {

        if (stopped == false) {
            handler.removeCallbacks(runnable)
            stopped = true
            stopButton.text = "Reset"
            startButton.isEnabled = false
            startButton.visibility = View.INVISIBLE

        } else {

            stopped = false
            stopButton.text = "Stop"

            milisecondsTime = 0L
            startTime = 0L
            currentTime = 0L
            newTime = 0L
            seconds = 0
            minutes = 0
            miliSeconds = 0
            hours = 0

            textMinutes.text = "00:00"
            textHours.text = "00:"
            textMilliseconds.text = ":00"
            timerRuns = false
            startButton.text = "Start"
            startButton.isEnabled = true
            startButton.visibility = View.VISIBLE
        }
    }
}
